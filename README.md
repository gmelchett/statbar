# statbar - A Tmux/DWM status bar daemon

`statbar` is a daemon for the tmux/DWN status bar. Instead of having a script that collects data all too often this
`statbar` collects different data att different intervals.
Everything is very configureable in a json file.

**UPDATE:** Nerdfont usage + default DWM status bar enabled. config file moved! Volume added

![alt text](screenshot.png)

From left:

* Sunrise
* Sunset
* Temperature (Celsius)
* Kind of weather
* Amount of rain (Only shown when raining)
* The block character shows cloud coverage in 8 steps
* The next block character shows air humitidy, also in 8 stages
* Air pressure in hPa
* Wind direction (the down arrow)
* Wind speed in m/s
* Memory usage (M: prefix) in percent
* Cpu load (C: prefix) in percent
* Battery status, if a battery is present (B: prefix) in percent
* Free diskspace
* wifi quality, if on wifi network
* 0K network traffic at the moment
* Date
* Time

## Configuration
1. Start statbar, it will create a default config that is disabled by default.
2. Edit ~/.config/gmelchegtt/statbar/config.json
3. Change the first option `configured` to `true`
4. In the `order` section, you can change the display order of the features. You can also remove lines, to not display that information
5. Install netcat
5. In your ~/.tmux.conf, add/change to `set -g status-right '#(nc localhost 1978)'`. You might need to set the tmux status bar right length, `set -g status-right-length 100`
6. `pkill statbar` and restart statbar and restart tmux (or reload tmux config)

## Limitations
  * The weather data is taken from Swedish Meteorological and Hydrological Institute. I guess that means that there is only weather data available for Sweden.

## Licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
