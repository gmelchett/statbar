/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2018-2022
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

// #cgo LDFLAGS: -lX11 -lasound
// #include "getvol.h"
// #include <X11/Xlib.h>
import "C"

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/OpenPeeDeeP/xdg"
	"github.com/firstrow/tcp_server"
	"github.com/kelvins/sunrisesunset"
	"github.com/mitchellh/go-homedir"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
)

const CONFIG_FILE = "config.json"

var xdgh = xdg.New("gmelchett", "statbar")

type BasicConfig struct {
	Prefix string `json:"prefix"`
	Bg     int    `json:"bg"`
	Fg     int    `json:"fg"`
}

type SunConfig struct {
	FgSunRise    int `json:"fg_sunrise"`
	BgSunRise    int `json:"bg_sunrise"`
	FgSunSet     int `json:"fg_sunset"`
	BgSunSet     int `json:"bg_sunset"`
	FgPlusDelta  int `json:"fg_plus_delta"`
	BgPlusDelta  int `json:"bg_plus_delta"`
	FgMinusDelta int `json:"fg_minus_delta"`
	BgMinusDelta int `json:"bg_minux_delta"`
}

type WeatherConfig struct {
	BgDesc int `json:"bg_desc"`
	FgDesc int `json:"fg_desc"`
	BgRain int `json:"bg_rain"`
	FgRain int `json:"fg_rain"`

	BgCloud    int `json:"bg_cloudy"`
	BgHumidity int `json:"bg_humidity"`
	BgThunder  int `json:"bg_thunder"`
	FgThunder  int `json:"fg_thunder"`

	BgTemp      int `json:"bg_temp"`
	FgTemp      int `json:"fg_temp"`
	BgPres      int `json:"bg_pres"`
	FgPres      int `json:"fg_pres"`
	BgWindDir   int `json:"bg_winddir"`
	FgWindDir   int `json:"fg_winddir"`
	BgWindSpeed int `json:"bg_windspeed"`
	FgWindSpeed int `json:"fg_windspeed"`
}

type NetworkConfig struct {
	Prefix string `json:"prefix"`
	Bg     int    `json:"bg"`
	Fg     int    `json:"fg"`
	Iface  string `json:"iface"`
}

type Config struct {
	Configured  bool     `json:"configured"`
	Latitude    float64  `json:"latitude"`
	Longitude   float64  `json:"longitude"`
	NetworkPort int      `json:"networkport"`
	BlueScale   [9]int   `json:"bluescale"`
	GrayScale   [9]int   `json:"grayscale"`
	Order       []string `json:"order"`
	DWM         []string `json:"dwm"`

	Sun       SunConfig     `json:"sun"`
	Weather   WeatherConfig `json:"weather"`
	CpuLoad   BasicConfig   `json:"cpuload"`
	MemUsage  BasicConfig   `json:"memusage"`
	SwapUsage BasicConfig   `json:"swapusage"`
	DiskUsage BasicConfig   `json:"diskusage"`
	Battery   BasicConfig   `json:"battery"`
	Wireless  BasicConfig   `json:"wireless"`
	Network   NetworkConfig `json:"network"`
	Date      BasicConfig   `json:"date"`
	Time      BasicConfig   `json:"time"`
	Volume    BasicConfig   `json:"volume"`
}

var configDefault = &Config{Configured: false,
	Latitude:    55.775313,
	Longitude:   13.511929,
	NetworkPort: 1978,
	BlueScale:   [9]int{17, 18, 19, 20, 21, 25, 26, 68, 69},
	GrayScale:   [9]int{0, 247, 248, 249, 250, 251, 252, 253, 254},
	DWM:         []string{"volume", "cpuload", "battery", "date", "time"},
	Order:       []string{"sun", "weather", "memusage", "swapusage", "cpuload", "battery", "diskusage", "wireless", "network", "date", "time"},

	Sun: SunConfig{
		FgSunRise:    186,
		BgSunRise:    235,
		FgSunSet:     210,
		BgSunSet:     235,
		FgPlusDelta:  119,
		BgPlusDelta:  235,
		FgMinusDelta: 199,
		BgMinusDelta: 235,
	},
	Weather: WeatherConfig{
		BgDesc: 235,
		FgDesc: 247,
		BgRain: 235,
		FgRain: 156,

		BgCloud:    235,
		BgHumidity: 235,
		BgThunder:  235,
		FgThunder:  0,

		BgTemp:      235,
		FgTemp:      63,
		BgPres:      235,
		FgPres:      45,
		BgWindDir:   235,
		FgWindDir:   152,
		BgWindSpeed: 235,
		FgWindSpeed: 156,
	},
	Network: NetworkConfig{
		Bg:    235,
		Fg:    45,
		Iface: "auto",
	},
	DiskUsage: BasicConfig{
		Prefix: "\uF6B7",
		Bg:     235,
		Fg:     71,
	},
	Battery: BasicConfig{
		Bg: 235,
		Fg: -1,
	},
	MemUsage: BasicConfig{
		Prefix: "\uf85a",
		Bg:     235,
		Fg:     -1,
	},
	SwapUsage: BasicConfig{
		Prefix: "\uf9e0",
		Bg:     235,
		Fg:     -1,
	},

	CpuLoad: BasicConfig{
		Prefix: "\uf44e",
		Bg:     235,
		Fg:     -1,
	},
	Wireless: BasicConfig{
		Bg: 235,
		Fg: -1,
	},
	Date: BasicConfig{
		Bg: 235,
		Fg: 177,
	},
	Time: BasicConfig{
		Bg: -1,
		Fg: 114,
	},
	Volume: BasicConfig{
		Prefix: "\ufa7d",
		Bg:     -1,
		Fg:     104,
	},
}

var dpy = C.XOpenDisplay(nil)

func jsonOpen(file *string, w interface{}) (err error) {

	f, err := os.Open(*file)
	if err != nil {
		return
	}
	defer f.Close()
	if err := json.NewDecoder(f).Decode(w); err != nil {
		fmt.Println("Failed decoding json", err)
	}
	return
}

func configLoad() *Config {

	if err := createDir(xdgh.ConfigHome()); err != nil {
		fmt.Println("Failed creating config directory", err)
		os.Exit(1)
	}

	config := new(Config)

	configFile := filepath.Join(xdgh.ConfigHome(), CONFIG_FILE)

	if err := jsonOpen(&configFile, config); err == nil {
		return config
	}

	if jsonConfig, err := json.MarshalIndent(configDefault, "", "    "); err == nil {
		saveFile(configFile, jsonConfig)
	}
	return config
}

type Text struct {
	bg, fg int
	text   string
}

type Feature struct {
	id       string
	callback func(*Config) []Text
	interval time.Duration
	onceaday bool
}

func sunRiseSet(p *sunrisesunset.Parameters, day time.Time) (time.Time, time.Time, error) {

	_, offset := day.Zone()
	p.UtcOffset = float64(offset / (60 * 60))
	p.Date = time.Date(day.Year(), day.Month(), day.Day(), 0, 0, 0, 0, time.UTC)
	return p.GetSunriseSunset()
}

func sunShow(config *Config) []Text {
	p := sunrisesunset.Parameters{
		Latitude:  config.Latitude,
		Longitude: config.Longitude,
	}

	todaySunrise, todaySunset, err := sunRiseSet(&p, time.Now())

	if err != nil {
		return nil
	}

	yesterdaySunrise, yesterdaySunset, err := sunRiseSet(&p, time.Now().AddDate(0, 0, -1))

	if err != nil {
		return nil
	}

	sunstatus := make([]Text, 3)

	sunstatus[0] = Text{bg: config.Sun.BgSunRise,
		fg:   config.Sun.FgSunRise,
		text: fmt.Sprintf("%s ", todaySunrise.Format("15:04"))}

	sunstatus[1] = Text{bg: config.Sun.BgSunSet,
		fg:   config.Sun.FgSunSet,
		text: fmt.Sprintf("%s ", todaySunset.Format("15:04"))}

	todayLen := todaySunset.Sub(todaySunrise).Minutes()
	yesterdayLen := yesterdaySunset.Sub(yesterdaySunrise).Minutes()

	if yesterdayLen > todayLen {
		sunstatus[2] = Text{bg: config.Sun.BgMinusDelta,
			fg:   config.Sun.FgMinusDelta,
			text: fmt.Sprintf("%d ", -int(math.RoundToEven(yesterdayLen-todayLen)))}
	} else {
		sunstatus[2] = Text{bg: config.Sun.BgPlusDelta,
			fg:   config.Sun.FgPlusDelta,
			text: fmt.Sprintf("%d ", int(math.RoundToEven(todayLen-yesterdayLen)))}
	}
	// TODO: Sun's angle at noon

	return sunstatus
}

type SMHIParameter struct {
	Name      string    `json:"name"`
	LevelType string    `json:"levelType"`
	Level     int       `json:"level"`
	Unit      string    `json:"unit"`
	Values    []float64 `json:"values"`
}

type SMHIWeather struct {
	ApprovedTime  time.Time `json:"approvedTime"`
	ReferenceTime time.Time `json:"referenceTime"`
	Geometry      struct {
		Type        string      `json:"type"`
		Coordinates [][]float64 `json:"coordinates"`
	} `json:"geometry"`
	TimeSeries []struct {
		ValidTime  time.Time       `json:"validTime"`
		Parameters []SMHIParameter `json:"parameters"`
	} `json:"timeSeries"`
}

func shmiDownload(lat, lon float64) (b []byte, err error) {
	smhi := http.Client{Timeout: time.Second * 5}

	url := fmt.Sprintf("https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/%.6f/lat/%.6f/data.json", lon, lat)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	resp, err := smhi.Do(req)

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}

var QUALITYSCALE = [...]int{84, 83, 82, 185, 216, 210, 173, 203, 9}

const MAX_WEATHER_AGE = 120

var WEATHER_DESCRIPTION = [...]string{
	"Clear sky",
	"Nearly clear sky",
	"Variable cloudiness",
	"Halfclear sky",
	"Cloudy sky",
	"Overcast",
	"Fog",
	"Light rain showers",
	"Moderate rain showers",
	"Heavy rain showers",
	"Thunderstorm",
	"Light sleet showers",
	"Moderate sleet showers",
	"Heavy sleet showers",
	"Light snow showers",
	"Moderate snow showers",
	"Heavy snow showers",
	"Light rain",
	"Moderate rain",
	"Heavy rain",
	"Thunder",
	"Light sleet",
	"Moderate sleet",
	"Heavy sleet",
	"Light snowfall",
	"Moderate snowfall",
	"Heavy snowfall"}

var BLOCKS = [...]string{
	" ",
	"\u2581",
	"\u2582",
	"\u2583",
	"\u2584",
	"\u2585",
	"\u2586",
	"\u2587",
	"\u2588",
	"\u2589"}

var ARROWS = [...]string{
	"\u2190",
	"\u2196",
	"\u2191",
	"\u2197",
	"\u2192",
	"\u2198",
	"\u2193",
	"\u2199"}

func saveFile(file string, d []byte) {
	if f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0644); err == nil {
		f.Write(d)
		f.Close()
	} else {
		fmt.Println("Failed to save file:"+file, err)
	}
}

func weatherSmhiShow(config *Config) []Text {
	if err := createDir(xdgh.CacheHome()); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	w := new(SMHIWeather)

	forecastFile := filepath.Join(xdgh.CacheHome(), "forecast.json")

	if _, err := os.Stat(forecastFile); err != nil {
		if d, err := shmiDownload(config.Latitude, config.Longitude); err == nil {
			saveFile(forecastFile, d)
		}
	}

	jsonOpen(&forecastFile, w)

	age := time.Now().Sub(w.ReferenceTime).Minutes()
	if age > MAX_WEATHER_AGE {
		if d, err := shmiDownload(config.Latitude, config.Longitude); err == nil {
			saveFile(forecastFile, d)
		}
		jsonOpen(&forecastFile, w)
	}

	weathermap := make(map[string]SMHIParameter)

	for _, weather := range w.TimeSeries {
		age := weather.ValidTime.Sub(time.Now())
		if age < 0.0 {
			continue
		}
		for _, param := range weather.Parameters {
			weathermap[param.Name] = param
		}
		break
	}

	weatherstatus := []Text{}

	// Weather description
	if param, ok := weathermap["Wsymb2"]; ok {

		weatherstatus = append(weatherstatus,
			Text{bg: config.Weather.BgDesc, fg: config.Weather.FgDesc,
				text: fmt.Sprintf("%s ", WEATHER_DESCRIPTION[int(param.Values[0])])})
	}
	// Rain/snow in mm
	if param, ok := weathermap["pmean"]; ok && param.Values[0] > 0 {
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgRain, fg: config.Weather.FgRain,
			text: fmt.Sprintf("%.1f mm ", param.Values[0])})
	}
	// How cloudy between, 0-8
	if param, ok := weathermap["tcc_mean"]; ok {
		idx := int(param.Values[0])
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgCloud, fg: config.GrayScale[idx],
			text: fmt.Sprintf("%s", BLOCKS[idx])})
	}
	// Air humidity, %
	if param, ok := weathermap["r"]; ok {
		idx := int(param.Values[0] / 12.5)
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgHumidity, fg: config.BlueScale[idx],
			text: fmt.Sprintf("%s", BLOCKS[idx])})
	}
	// Thunder %
	if param, ok := weathermap["tstm"]; ok && param.Values[0] > 12.5 {
		idx := int(param.Values[0] / 12.5)
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgThunder, fg: config.Weather.FgThunder,
			text: fmt.Sprintf("%s", BLOCKS[idx])})
	}

	// Temperature
	if param, ok := weathermap["t"]; ok {
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgTemp, fg: config.Weather.FgTemp,
			text: fmt.Sprintf("%.1f\ue339 ", param.Values[0])})
	}
	// Pressure
	if param, ok := weathermap["msl"]; ok {
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgPres, fg: config.Weather.FgPres,
			text: fmt.Sprintf("%d ", int(param.Values[0]+0.5))})
	}
	// Wind direction
	if param, ok := weathermap["wd"]; ok {
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgWindDir, fg: config.Weather.FgWindDir,
			text: fmt.Sprintf("%s", ARROWS[int(param.Values[0]/(360.0/8.0))])})
	}
	// Wind speed
	if param, ok := weathermap["ws"]; ok {
		weatherstatus = append(weatherstatus, Text{bg: config.Weather.BgWindSpeed, fg: config.Weather.FgWindSpeed,
			text: fmt.Sprintf("%.1f ", param.Values[0])})
	}

	return weatherstatus
}

func cpuLoadShow(config *Config) []Text {

	lvlTxt := "?"
	lvlNum := 0.0
	if load, err := cpu.Percent(15*time.Second, false); err == nil {
		lvlNum = load[0]
		lvlTxt = fmt.Sprintf("%d%%", int(math.RoundToEven(lvlNum)))
	}
	return []Text{
		Text{bg: config.CpuLoad.Bg,
			fg: QUALITYSCALE[int(math.RoundToEven(float64(len(QUALITYSCALE))*lvlNum/100.0))],
			text: fmt.Sprintf("%s%s ", config.CpuLoad.Prefix,
				lvlTxt)},
	}

}

func memUsageShow(config *Config) []Text {

	if stat, err := mem.VirtualMemory(); err == nil {
		return []Text{
			Text{bg: config.MemUsage.Bg, fg: QUALITYSCALE[int(stat.UsedPercent/12.5)],
				text: fmt.Sprintf("%s%d%% ", config.MemUsage.Prefix, int(math.RoundToEven(stat.UsedPercent)))},
		}

	}
	return nil
}

func swapUsageShow(config *Config) []Text {

	if stat, err := mem.SwapMemory(); err == nil {
		return []Text{
			Text{
				bg: config.SwapUsage.Bg,
				fg: QUALITYSCALE[int(math.RoundToEven(float64(len(QUALITYSCALE))*stat.UsedPercent/100.0))],
				text: fmt.Sprintf("%s%d%% ", config.SwapUsage.Prefix,
					int(math.RoundToEven(stat.UsedPercent)))},
		}
	}
	return nil
}

func diskUsageShow(config *Config) []Text {

	d, err := homedir.Dir()
	if err != nil {
		return nil
	}

	if usage, err := disk.Usage(d); err == nil {
		space := usage.Free / (1024 * 1024)

		t := Text{bg: config.DiskUsage.Bg, fg: config.DiskUsage.Fg}
		t.text = fmt.Sprintf("%s", config.DiskUsage.Prefix)

		if space < 1024 {
			t.text += fmt.Sprintf("%dM ", space)
		} else if space < 10*1024 {
			t.text += fmt.Sprintf("%.1fG ", float64(space)/1024.0)
		} else if space < 1000*1024 {
			t.text += fmt.Sprintf("%dG ", space/1024)
		} else {
			t.text += fmt.Sprintf("%.2fT ", float64(space)/1024.0/1000.0)
		}
		return []Text{t}
	}
	return nil
}

type batStatus struct {
	Charging bool
	Current  int
	Full     int
	Error    bool
}

func batteryRead() []batStatus {

	bats, err := filepath.Glob("/sys/class/power_supply/BAT*")

	if err != nil {
		return []batStatus{}
	}

	bs := make([]batStatus, 0, len(bats))

	for i := range bats {

		fullstr, err := ioutil.ReadFile(filepath.Join(bats[i], "energy_full"))
		if err != nil {
			bs = append(bs, batStatus{Error: true})
			continue
		}
		nowstr, err := ioutil.ReadFile(filepath.Join(bats[i], "energy_now"))
		if err != nil {
			bs = append(bs, batStatus{Error: true})
			continue
		}

		status, err := ioutil.ReadFile(filepath.Join(bats[i], "status"))
		if err != nil {
			bs = append(bs, batStatus{Error: true})
			continue
		}

		full, err := strconv.ParseUint(string(fullstr[:len(fullstr)-1]), 10, 64)
		if err != nil {
			bs = append(bs, batStatus{Error: true})
			continue
		}

		now, err := strconv.ParseUint(string(nowstr[:len(nowstr)-1]), 10, 64)
		if err != nil {
			bs = append(bs, batStatus{Error: true})
			continue
		}

		bs = append(bs, batStatus{Error: false,
			Charging: string(status) == "Charging\n",
			Current:  int(now),
			Full:     int(full),
		})
	}
	return bs
}

func batteryStatusShow(config *Config) []Text {

	// empty --> full
	batteryLevel := []string{"\uF58D", "\uF579", "\uF57A", "\uF57B", "\uF57C", "\uF57D", "\uF57E", "\uF57F", "\uF580", "\uF581", "\uF578"}

	batteries := batteryRead()

	if len(batteries) == 0 {
		return nil
	}

	var t string

	total := 0
	on_ac := false
	charging := false

	if ac_adapter, err := ioutil.ReadFile("/sys/class/power_supply/AC/online"); err == nil && string(ac_adapter) == "1\n" {
		on_ac = true
	}

	for i := range batteries {
		if batteries[i].Error {
			t += "\uf590"
			continue
		}

		p := int(100.0*float64(batteries[i].Current)/float64(batteries[i].Full) + 0.5)
		total += p
		if on_ac && p == 100 {
			continue
		}
		if batteries[i].Charging {
			charging = true
		}

		t += batteryLevel[p*(len(batteryLevel)-1)/100]
		t += fmt.Sprintf("%d%%", p)
	}

	t += " "
	if on_ac {
		if charging {
			t = "\uF740" + t
		} else {
			t = "\uFBA3" + t
		}
	}

	return []Text{
		Text{bg: config.Battery.Bg,
			fg:   QUALITYSCALE[8-int(float64(total/len(batteries))/12.5)],
			text: t},
	}
}

func wirelessShow(config *Config) []Text {

	var wm *regexp.Regexp
	var l string
	var reader *bufio.Reader

	f, err := os.Open("/proc/net/wireless")

	if err != nil {
		goto failOut
	}

	defer f.Close()

	reader = bufio.NewReader(f)
	reader.ReadString('\n')
	reader.ReadString('\n')

	l, err = reader.ReadString('\n')

	if err != nil {
		goto failOut2
	}

	wm = regexp.MustCompile(`^\s*\S*:\s+\d+\s+(\d+)`)

	if m := wm.FindAllStringSubmatch(l, -1); m != nil {
		if qual, err := strconv.Atoi(m[0][1]); err == nil {
			q := int((100.0 * float64(qual) / 80.0) / 12.5)
			return []Text{Text{bg: config.Wireless.Bg,
				fg:   QUALITYSCALE[8-q],
				text: "\uFAA8" + BLOCKS[q] + " "}}
		}
	}
failOut2:
	return []Text{Text{bg: config.Wireless.Bg, fg: 1, text: "\uFAA9 "}}
failOut:
	return []Text{Text{bg: config.Wireless.Bg, fg: 199, text: ""}}
}

type netStatus struct {
	first    bool
	iface    []string
	prevRXTX []uint64
}

var netstatus netStatus

func findiface(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if f := filepath.Base(path); f != "lo" && f != "net" {
		netstatus.iface = append(netstatus.iface, f)
		netstatus.prevRXTX = append(netstatus.prevRXTX, 0)
	}
	return nil
}

func networkShow(config *Config) []Text {

	if netstatus.iface == nil {
		netstatus.first = true
		netstatus.iface = make([]string, 0, 3)
		netstatus.prevRXTX = make([]uint64, 0, 3)
		if config.Network.Iface == "auto" {
			filepath.Walk("/sys/class/net/", findiface)
		} else {
			netstatus.iface = append(netstatus.iface, config.Network.Iface)
		}
	}

	rxtx := make([]uint64, len(netstatus.iface))

	for i := range netstatus.iface {

		txFile := fmt.Sprintf("/sys/class/net/%s/statistics/tx_bytes", netstatus.iface[i])
		rxFile := fmt.Sprintf("/sys/class/net/%s/statistics/rx_bytes", netstatus.iface[i])

		f_tx, err := os.Open(txFile)

		if err != nil {
			continue
		}
		defer f_tx.Close()

		f_rx, err := os.Open(rxFile)

		if err != nil {
			continue
		}
		defer f_rx.Close()

		var rx, tx uint64

		if count, err := fmt.Fscanf(f_tx, "%d\n", &tx); count != 1 || err != nil {
			continue
		}

		if count, err := fmt.Fscanf(f_rx, "%d\n", &rx); count != 1 || err != nil {
			continue
		}
		rxtx[i] = tx + rx
	}

	toDiv := uint64(0)
	totalrxtx := uint64(0)
	prevTotalRXTX := uint64(0)
	for i := range rxtx {
		if rxtx[i] > 0 && rxtx[i] >= netstatus.prevRXTX[i] {
			totalrxtx += rxtx[i]
			prevTotalRXTX += netstatus.prevRXTX[i]
			toDiv++
		}
		netstatus.prevRXTX[i] = rxtx[i]
	}

	speed := " "

	if toDiv > 0 && !netstatus.first {
		d := int((totalrxtx - prevTotalRXTX) / toDiv / 1024)

		if d < 1024 {
			speed = fmt.Sprintf("%dK ", int(d))
		} else {
			speed = fmt.Sprintf("%.1fM ", float64(d)/1024)
		}
	}
	netstatus.first = false

	return []Text{Text{bg: config.Network.Bg,
		fg:   config.Network.Fg,
		text: config.Network.Prefix + speed}}
}

func timeShow(config *Config) []Text {
	return []Text{
		Text{bg: config.Time.Bg, fg: config.Time.Fg, text: fmt.Sprintf("%s ", time.Now().Format("15:04"))},
	}
}

func dateShow(config *Config) []Text {
	return []Text{
		Text{bg: config.Date.Bg, fg: config.Date.Fg, text: fmt.Sprintf("%s ", time.Now().Format("Mon _2 Jan"))},
	}
}

func volumeShow(config *Config) []Text {
	return []Text{
		Text{bg: config.Volume.Bg, fg: config.Volume.Fg,
			text: fmt.Sprintf("%s%d ", config.Volume.Prefix, int(C.get_volume_perc()))},
	}
}

func dwmSetStatus(status string) {
	C.XStoreName(dpy, C.XDefaultRootWindow(dpy), C.CString(status))
	C.XSync(dpy, 1)
}

func repeat(ch chan map[string][]Text, f Feature, config *Config) {

	if !f.onceaday {
		if f.interval > 0 {
			for {
				ch <- map[string][]Text{f.id: f.callback(config)}
				time.Sleep(f.interval)
			}
		}
	} else {
		ch <- map[string][]Text{f.id: f.callback(config)}
		then := time.Now()
		for {
			time.Sleep(time.Minute)
			now := time.Now()
			if now.Day() != then.Day() {
				ch <- map[string][]Text{f.id: f.callback(config)}
				then = now
			}
		}
	}
}

type statusbar struct {
	configured bool
	status     *strings.Builder
}

func (status *statusbar) statusRead(c *tcp_server.Client) {
	if status.configured {
		c.Send(strings.TrimSpace(status.status.String()))
	} else {
		c.Send("Not configured! Edit " + filepath.Join(xdgh.ConfigHome(), CONFIG_FILE))
	}
	c.Close()
}

func createDir(dir string) (err error) {

	if stat, err := os.Stat(dir); err != nil || !stat.IsDir() {
		err = os.MkdirAll(dir, 0755)
	}
	return
}

func main() {
	features := []Feature{
		Feature{
			id:       "time",
			callback: timeShow,
			interval: 30 * time.Second,
		},
		Feature{
			id:       "date",
			callback: dateShow,
			onceaday: true,
		},
		Feature{
			id:       "sun",
			callback: sunShow,
			onceaday: true,
		},
		Feature{
			id:       "weather",
			callback: weatherSmhiShow,
			interval: 20 * time.Minute,
		},
		Feature{
			id:       "memusage",
			callback: memUsageShow,
			interval: 15 * time.Second,
		},
		Feature{
			id:       "swapusage",
			callback: swapUsageShow,
			interval: 15 * time.Second,
		},
		Feature{
			id:       "diskusage",
			callback: diskUsageShow,
			interval: 15 * time.Second,
		},
		Feature{
			id:       "cpuload",
			callback: cpuLoadShow,
			interval: 15 * time.Second,
		},
		Feature{
			id:       "battery",
			callback: batteryStatusShow,
			interval: 1 * time.Minute,
		},
		Feature{
			id:       "wireless",
			callback: wirelessShow,
			interval: 30 * time.Second,
		},
		Feature{
			id:       "network",
			callback: networkShow,
			interval: 15 * time.Second,
		},
		Feature{
			id:       "volume",
			callback: volumeShow,
			interval: 3 * time.Second,
		},
	}

	log.SetOutput(ioutil.Discard) // tcp_server prints pointless startup print

	config := configLoad()

	if !config.Configured {
		fmt.Println("Not configured! Edit " + filepath.Join(xdgh.ConfigHome(), CONFIG_FILE))
	}

	status := statusbar{configured: config.Configured,
		status: new(strings.Builder)}

	ch := make(chan map[string][]Text, len(features))
	current := make(map[string][]Text)

	for _, v := range config.Order {
		found := false
		for _, k := range features {
			if v == k.id {
				go repeat(ch, k, config)
				found = true
				break
			}
		}
		if !found {
			fmt.Println("Unknown feature:", v)
			os.Exit(1)
		}
	}

	server := tcp_server.New(fmt.Sprintf("localhost:%d",
		config.NetworkPort))

	server.OnNewClient(status.statusRead)
	go server.Listen()

	for {
		m := <-ch

		if m == nil {
			continue
		}

		for k, v := range m {
			current[k] = v
		}

		s := new(strings.Builder)
		bg := -1
		fg := -1

		for _, key := range config.Order {
			for _, t := range current[key] {
				if bg != t.bg && t.bg != -1 {
					bg = t.bg
					s.WriteString(fmt.Sprintf("#[bg=colour%d]", bg))
				} else if bg == -1 {
					s.WriteString(fmt.Sprintf("#[bg=colour0]"))
					bg = 0
				}

				if fg != t.fg && t.fg != -1 {
					fg = t.fg
					s.WriteString(fmt.Sprintf("#[fg=colour%d]", fg))
				}
				s.WriteString(t.text)
			}
		}
		status.status = s

		// TODO: 0-16 and > 232
		// rgb_R = rgb_G = rgb_B = (number - 232) * 10 + 8
		R := func(number int) int { return ((number - 16) / 36) * 51 }
		G := func(number int) int { return (((number - 16) % 36) / 6) * 51 }
		B := func(number int) int { return ((number - 16) % 6) * 51 }

		if len(config.DWM) > 0 {
			var st strings.Builder

			for i := range config.DWM {
				for _, t := range current[config.DWM[i]] {

					if bg != t.bg && t.bg != -1 {
						bg = t.bg
						st.WriteString(fmt.Sprintf("\\033[38;1;%d;%d;%dm", R(bg), G(bg), B(bg)))
					} else if bg == -1 {
						st.WriteString(fmt.Sprintf("\\033[0m"))
						bg = 0
					}

					if fg != t.fg && t.fg != -1 {
						fg = t.fg
						st.WriteString(fmt.Sprintf("\\033[38;2;%d;%d;%dm", R(fg), G(fg), B(fg)))
					}
					st.WriteString(t.text)
				}
				dwmSetStatus(st.String())
			}
		}
	}
}
