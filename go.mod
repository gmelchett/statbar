module statbar

go 1.12

require (
	github.com/OpenPeeDeeP/xdg v0.2.0
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/firstrow/tcp_server v0.0.0-20180905125330-662e42594111
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/kelvins/sunrisesunset v0.0.0-20170601204625-14f1915ad4b4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/shirou/gopsutil v2.18.12+incompatible
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	golang.org/x/sys v0.0.0-20190316082340-a2f829d7f35f // indirect
)
